var mongoose = require('mongoose')
var Schema = mongoose.Schema
var SpaceSchema = new Schema({
  name:{type:String,required:true},
  totalTickets:{type:Number,required:true,default:0},
  services:[{type: Schema.Types.ObjectId, ref: 'Service'}],
  account:{type:Number,required:true},
  createdBy:{type:String,default:'Admin'},
  createdOn:{type:Date,default:Date.now()}
})

module.exports = mongoose.model('Space',SpaceSchema)
