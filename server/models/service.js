var mongoose = require('mongoose')
var Schema = mongoose.Schema
var ServiceSchema = new Schema({
  name:{type:String,required:true},
  allocatedTickets:{type:Number,required:true,default:0},
  guestsInQueue:[{type: Schema.Types.ObjectId,ref: 'Guest'}],
  inQueue:{type:Number,required:false},
  availableTickets:{type:Number,required:false},
  account:{type:Number,required:true}
})

module.exports = mongoose.model('Service',ServiceSchema)
