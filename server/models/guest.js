var mongoose = require('mongoose')
var Schema = mongoose.Schema
var GuestSchema = new Schema({
  fullName:{type:String,required:true},
  mobileNumber:{type:String,required:true},
  password:{type: String,required:true},
  status:{type:Boolean,required:true,default:false}
})

module.exports = mongoose.model('Guest',GuestSchema)
