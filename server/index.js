var express = require('express')
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var config = require('./configuration/config')
var mongoURL = `mongodb://${config.mLabUser}:${config.mLabPassword}@ds143971.mlab.com:43971/dorongo`
mongoose.connect(mongoURL)
mongoose.Promise = global.Promise

var app = express()

var port = 3000
var service = require('./routers/serviceRoute')
var guest = require('./routers/guestRoute')
var space = require('./routers/spaceRoute')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

app.use('/space',space)
app.use('/service',service)
app.use('/guest',guest)


app.listen(port,function(){
  console.log('App listening on port '+port)
})
