var express = require('express')
var router = express.Router()
var controller = require('../controllers/serviceController')

router.post('/add/:spaceId',controller.add_service)
router.get('/delete/:serviceId',controller.delete_service)
router.get('/:serviceId/add_guest/:guestId',controller.check_in);
router.get('/:serviceId/remove_guest/:guestId',controller.check_out);

module.exports = router;
