var express = require('express')
var router = express.Router()
var controller = require('../controllers/spaceController')

router.post('/add',controller.add_space);
router.get('/delete/:spaceId',controller.delete_space);
router.get('/',controller.list_spaces)
module.exports = router;
