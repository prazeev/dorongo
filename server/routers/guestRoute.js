var express = require('express')
var router = express.Router()
var controller = require('../controllers/guestController')

router.post('/add',controller.add_guest)
router.get('/delete/:guestId',controller.delete_guest)

module.exports = router;
