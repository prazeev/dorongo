var Space = require('../models/space')
var Service = require('../models/service')
exports.add_space = function(req,res){
  var name = req.body.name;
  var totalTickets = req.body.totalTickets;
  var account=req.body.account;
  if(!name||!totalTickets||!account){
    return res.json({
      error:true,
      message:"Enter all details"
    })
  }
  var newSpace = new Space({
    name:name,
    totalTickets:totalTickets,
    account:account
  })
  newSpace.save(function(err,space){
    res.send(space)
  })
}
exports.delete_space = async function(req,res){
  var spaceId = req.params.spaceId;
  var space = await Space.findOne({_id:spaceId})
  var servicesInSpace = space.services
  for(i=0;i<servicesInSpace.length;i++){
    await Service.deleteOne({_id:servicesInSpace[i]})
  }
  await Space.deleteOne({_id:spaceId},function(err,space){
    if(!err){
      res.json("Deleted"+space)
    }
    else{
      res.json({
        err:err
      })
    }
  })

}
exports.list_spaces = function(req,res){
  Space.find({},function(err,spaces){
    res.json(spaces)
  })
}
