var Service = require('../models/service')
var Space = require('../models/space')
var Guest = require('../models/guest')
exports.add_service = async function(req, res) {
  var spaceId = req.params.spaceId;
  var allocatedTickets = req.body.allocatedTickets;
  var availableTickets = allocatedTickets
  var ticketsInServices = 0;
  const space = await Space.findById(spaceId).populate('services')
  if (!space) {
    res.json({
      error: true,
      message: "Space doesnot exist"
    })
  } else {
    const servicesInSpace = space.services
    for (i = 0; i < servicesInSpace.length; i++) {
      ticketsInServices += servicesInSpace[i].allocatedTickets;
    }
    var totalTicketsInSpace = space.totalTickets;
    var remainingTickets = totalTicketsInSpace - ticketsInServices
    if (remainingTickets < allocatedTickets) {
      res.json({
        error: true,
        message: "Not enough tickets in space"
      })
    } else {
      var newService = await new Service({
        name: req.body.name,
        allocatedTickets: allocatedTickets,
        availableTickets:allocatedTickets
      });
      newService.account = space.account;
      newService.inQueue = 0;
      await newService.save()
      space.services.push(newService)
      await space.save((err, space) => {
        res.json({
          message: "Service added successfully",
          service: newService
        })
      })
    }
  }
}
exports.delete_service = async function(req, res) {
  var serviceId = req.params.serviceId;
  await Service.deleteOne({
    _id: serviceId
  });
  res.json({
    message: "Service Deleted Successfully",
    serviceId: serviceId
  })
}
exports.check_out = async function(req, res) {
  try {
    var index;
    var guestId = req.params.guestId;
    var serviceId = req.params.serviceId;
    var service = await Service.findById(serviceId).populate('guestsInQueue')
    var guestsArray = service.guestsInQueue;
    for (i = 0; i < guestsArray.length; i++) {
      if (guestId == guestsArray[i]._id) {
        guestsArray.splice(i,1)
        service.guestsInQueue = guestsArray;
        service.inQueue = guestsArray.length;
        service.availableTickets = service.allocatedTickets-service.inQueue
        await service.save()
        return res.json({
          message: "Guest removed successfully from service",
          guestId: guestId,
          serviceId: serviceId
        })
      } else {
        index = undefined
      }
    }
    if (index === undefined) {
      res.json({
        error: true,
        message: "Guest doesnot exist"
      })
    }
  } catch (err) {
    res.status(500).json({
      error: true,
      message: "Unexpected error!Try again"
    })
  }
}
exports.list_services = function(req, res) {
  Service.find({}, function(err, services) {
    res.json(services);
  })
}
exports.check_in = async function(req, res) {
  try {
    var guestId = req.params.guestId;
    var serviceId = req.params.serviceId;
    var guest = await Guest.findById(guestId);
    var service = await Service.findById(serviceId);
    if (!guest) {
      res.json({
        error: true,
        message: "Guest doesnot exist"
      })
    } else if (!service) {
      res.json({
        error: true,
        message: "Service doesnot exist"
      })
    } else {
      for (i = 0; i < service.guestsInQueue.length; i++) {
        if (guestId == service.guestsInQueue[i]._id) {
          return res.json({
            error: true,
            message: "Guest already exists"
          })
        }
      }
      service.guestsInQueue.push(guest);

      await service.save()
      var service = await Service.findById(serviceId).populate('guestsInQueue');
      var inQueue = service.guestsInQueue.length;
      service.inQueue = inQueue;
      service.availableTickets = service.allocatedTickets-service.inQueue
      await service.save()
      res.json({
        message: "Guest added to service",
        serviceId: serviceId,
        guestId: guestId
      })
    }
  } catch (err) {
    res.status(500).json({
      error: true,
      message: "Unexpected error!Try again"
    })
  }
}
