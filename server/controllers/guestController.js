var Guest = require('../models/guest')
var Service = require('../models/service')
exports.add_guest = async function(req,res){
  var serviceId = req.params.serviceId;
  var service = Service.findById(serviceId);
  var fullName = req.body.fullName
  var mobileNumber = req.body.mobileNumber;
  var password = req.body.password;
  var newGuest = new Guest({
    fullName:fullName,
    mobileNumber:mobileNumber,
    password:password
  })
  await newGuest.save();
  res.json({
    message:"Guest added successfully",
    guest:newGuest
  })
}

exports.delete_guest = async function(req,res){
  var guestId = req.params.guestId;
  await Guest.deleteOne({_id:guestId});
  res.json({
    message:"Guest deleted successfully",
    id:guestId
  })
}
